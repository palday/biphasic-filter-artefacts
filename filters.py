import pylab
import matplotlib.pyplot as plt
import mne
import numpy as np
import glob
import os.path
import pandas as pd

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay")
# kind="standard_1005"
# kind="standard_1020"
# kind="easycap-M10"
montage = mne.channels.read_montage(kind="standard_1020")


event_id = {'A-ist':41,
            'A-hat' :42,
            'B-ist':43,
            'B-hat' :44,
            'C-ist':45,
            'C-hat' :46,
            'D-ist':47,
            'D-hat' :48
            }

windows = {
        "N400std" : (300, 500),
        "LPCstd" : (600, 800),
        "N400paper" : (380, 530),
        "LPCpaper" : (750, 900)
        }

for lb in (0.16, 0.2, 0.3):
    for hb in (30,):
        for ir in ('iir','fft'):
            print "{}: {} to {} Hz".format(ir.upper(),lb,hb)
            epochs = {}
            evoked = {}
            for bl in (True,False): 
                if bl:
                    baseline = -0.2,0
                    blstr = "_200ms_baseline"
                    print "Baseline: 200ms prestimulus"
                else:
                    baseline = None
                    blstr = ""
                    print "Baseline: none"
                filtstr = "{}-{}_{}{}".format(ir,lb,hb,blstr)
                for i in xrange(1,35): # 35
                    subj = "{:02d}".format(i)
                    print "Subject {}".format(subj)
                    raw = mne.io.read_raw_brainvision(os.path.join("raw",subj,subj+".vhdr"),
                                                      eog=("H+","H_","V+","V_"),
                                                      misc=("A1","A2"),
                                                      montage=montage,
                                                      preload=True)
                    raw.filter(lb, hb,l_trans_bandwidth=0.15,n_jobs=1,method=ir)
                    raw = mne.io.add_reference_channels(raw,"A1")
                    raw = mne.io.set_eeg_reference(raw,["A1","A2"])[0]
                    events = raw.get_brainvision_events()
                    eventsq = events[:,2]
                    rating = np.zeros(len(eventsq),dtype=int)

                    # items are not encoded as part of the trigger sequence for this experiment!
                    for i,e in enumerate(eventsq):
                        if 11 <= e <= 14: # NP
                            rating[i] = eventsq[i+5]
                        elif 21 <= e <= 28: # aux
                            rating[i] = eventsq[i+4]
                        elif 31 <= e <= 38: # adverb
                            rating[i] = eventsq[i+3]
                        elif 41 <= e <= 48: # verb
                            rating[i] = eventsq[i+2]

                    events[:,1] = rating
                    # events without an extracted rating are filler, so we can ignore them
                    keep = np.array([bool(x) for x in events[:,1]])
                    events = events[keep]

                    picks = mne.pick_types(raw.info, eeg=True, eog=True, stim=False, misc=False)
                    tmin, tmax = -0.2, 1.2
                    reject = dict(eeg=75e-6, eog=250e-6)
                    epochs = mne.Epochs(raw, events,
                                        event_id,
                                        tmin, tmax,
                                        baseline=baseline,
                                        preload=True,
                                        reject=reject,
                                        picks=picks,
                                        on_missing='warning')
                    behavioral = epochs.events[:,1]
                    df = epochs.to_data_frame(picks=None, index=['epoch','time'],scale_time=1e3)
                    eeg_chs = [c for c in df.columns if c not in ('A1', 'A2', 'V+','V_','H+','H_','condition')]
                    factors = ['epoch','condition'] # the order is important here! otherwise the shortcut with items later won't  work
                    sel = factors + eeg_chs
                    df = df.reset_index()

                    retrieve = []
                    for w in windows:
                        temp = df[ df.time >= windows[w][0] ]
                        dfw = temp[ temp.time <= windows[w][1] ]
                        dfw_mean = dfw[sel].groupby(factors).mean()
                        dfw_mean["subj"] = subj
                        dfw_mean["judgement"] = behavioral
                        dfw_mean["win"] = "{}..{}".format(*windows[w])
                        dfw_mean["wname"] = w
                        retrieve.append(dfw_mean)

                    retrieve = pd.concat(retrieve)
                    retrieve.to_csv(os.path.join("data","{}_{}.csv".format(subj,filtstr)))

                    evoked[subj] = {}
                    for e in event_id:
                        epochs[e].drop_bad_epochs()
                        evoked[subj][e] = epochs[e].average()

                evoked_by_cond = {e:[evoked[s][e] for s in evoked if evoked[s][e].nave > 0] for e in event_id}
                grand_average = {cond:mne.grand_average(evoked_by_cond[cond]) for cond in evoked_by_cond}

                try:
                    import cPickle as pickle
                except ImportError:
                    import pickle

                with open("{}-cache.pickle".format(filtstr),"wb") as pfile:
                    pickle.dump(evoked,pfile,-1)
                    pickle.dump(evoked_by_cond,pfile,-1)
                    pickle.dump(grand_average,pfile,-1)
