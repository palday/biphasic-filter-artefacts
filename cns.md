---
title: Biphasic ERP responses are not just filter artifacts
author: 
  - Phillip M. Alday
  - Sabine Frenzel
  - Matthias Schlesewsky
  - Ina Bornkessel-Schlesewsky
date: November 2015
---

Filtering remains a poorly understood preprocessing step in studying event-related potentials (ERPs).
Several recent papers have highlighted the distortions that filtering can introduce into ERP timecourses.
Disconcertingly, Tanner et al. (2015) have demonstrated that high-pass filtering can introduce spurious components before a real effect, e.g. an N400 before a P600.
Given that filtering traditions vary between workgroups, this raises the question whether the different component patterns observed across languages for the same manipulation are an artifact. 
Here, we sought to examine the stability across filters of the biphasic N400-P600 pattern often observed in German-language data.

Reanalyzing data from Roehm et al. (2013), we looked at the impact of bandpass filtering with both infinite impulse-response (IIR) and finite impulse-response (FIR) filters. 
The upper edge of the passband was held constant (30Hz), while the lower edge was varied across 0.16, 0.2 and 0.3 Hz with a left transition-band width of 0.15 Hz. 
Using mixed-effects models, we examined the interaction of the experimental manipulation with passband and response type. 
Across filters, the previously observed biphasic pattern was preserved. 
Additionally, we observed an effect for both passband and response type.
Visually, the later components seemed to influence earlier components more with the IIR vs the FIR filter. 

The biphasic pattern is not an artifact of the choice of passband. 
Moreover, the choice of filter involves much more than the selection of the passband. 
Filter selection involves many tradeoffs and there is no one universal filter applicable to all (EEG) data. 
