# Copyright (c) 201516, Phillip Alday
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

filtererps = figures/erpcmp_motion_fft-016_30_200ms_baseline.pdf figures/erpcmp_motion_fft-02_30_200ms_baseline.pdf figures/erpcmp_motion_fft-03_30_200ms_baseline.pdf figures/erpcmp_motion_iir-016_30_200ms_baseline.pdf figures/erpcmp_motion_iir-02_30_200ms_baseline.pdf figures/erpcmp_motion_iir-03_30_200ms_baseline.pdf figures/erpdiff_motion_fft-016_30_200ms_baseline.pdf figures/erpdiff_motion_fft-02_30_200ms_baseline.pdf figures/erpdiff_motion_fft-03_30_200ms_baseline.pdf figures/erpdiff_motion_iir-016_30_200ms_baseline.pdf figures/erpdiff_motion_iir-02_30_200ms_baseline.pdf figures/erpdiff_motion_iir-03_30_200ms_baseline.pdf
filtercache = fft-0.16_30-cache.pickle fft-0.16_30_200ms_baseline-cache.pickle fft-0.2_30-cache.pickle fft-0.2_30_200ms_baseline-cache.pickle fft-0.3_30-cache.pickle fft-0.3_30_200ms_baseline-cache.pickle iir-0.16_30-cache.pickle iir-0.16_30_200ms_baseline-cache.pickle iir-0.2_30-cache.pickle iir-0.2_30_200ms_baseline-cache.pickle iir-0.3_30-cache.pickle iir-0.3_30_200ms_baseline-cache.pickle 

.PHONY: cns-draft clean superclean

cns-draft: poster-cns.pdf 
	cp poster-cns.pdf poster-cns_`git show -s --format=%ci HEAD | awk '{print $$1}'`_`git rev-parse --short HEAD`.pdf

poster-cns.pdf: poster-cns.tex $(filtererps) figures/rousselet2012-fig01.pdf cns.bib beamerthemeRJHlandscapeUniSA.sty figures/cnl-color-white.png figures/unisa-landscape.eps figures/poster-cns-url.eps
	latexmk -pdf -silent poster-cns.tex

$(filtererps): filters_figures.py $(filtercache)
	python filters_figures.py
	
$(filtercache): filters.py
	python filters.py
	
cns.pdf : cns.md
	pandoc -o $@ $<

poster-sfn.pdf: poster-sfn.tex beamerthemeRJHlandscapeUniSA.sty sfn.bib figures/cnl-color-white.png figures/unisa-landscape.eps figures/poster-url.eps $(rfigs) $(pyfigs)
	latexmk -pdf poster-sfn.tex

figures/poster-cns-url.eps: generateQRcns.py
	python generateQRcns.py
	
clean: 
	rm -f $(filtererps)
	rm -f figures/*converted-to.pdf
	rm -f figures/poster-cns-url.eps
	rm -f Rplots.pdf
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.log *.nav *.snm *.toc *.synctex.gz

superclean: clean
	rm -f $(filtercache)